package pl.sda.example.restclient.service;

import pl.sda.example.restclient.http.client.model.ToDo;

import java.util.List;

public interface ToDoService {

    ToDo getToDo(Long id);

    List<ToDo> getToDos();

    void addToDo(ToDo todo);

    void updateToDo(ToDo todo);

}
