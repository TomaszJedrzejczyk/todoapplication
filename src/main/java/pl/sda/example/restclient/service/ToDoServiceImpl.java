package pl.sda.example.restclient.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.sda.example.restclient.http.client.model.ToDo;

import java.io.IOException;
import java.util.List;

@Service
public class ToDoServiceImpl implements ToDoService {

    private static HttpClient httpClient = HttpClientBuilder.create().build();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();

    private static final Logger logger = LoggerFactory.getLogger(ToDoServiceImpl.class);

    @Override
    public ToDo getToDo(Long id) {
        //URL pod którym będziemy wykonywać zapytania http
        String url = "https://jsonplaceholder.typicode.com/todos/" + id;
        HttpGet httpGet = new HttpGet(url); // mamy tutaj metodki GET, PUT, DELETE
        HttpResponse httpResponse = null;
        try {
            //wykonywanie zapytania httpGet
            httpResponse = httpClient.execute(httpGet);
            //Parsowanie odpowiedzi na obiek String - Json
            String toDoAsJson = EntityUtils.toString(httpResponse.getEntity());
            logger.info("Response code: {} for reuest: {}", httpResponse.getStatusLine().getStatusCode(), httpGet);
            logger.info(toDoAsJson);
            //Parsowanie Json na obiekt przy pomocy bibioteki Jackson 2
            ToDo toDo = objectMapper.readValue(toDoAsJson, ToDo.class);
            logger.info(toDo.toString());
            return toDo;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //Zamykanie odpowedzi
            HttpClientUtils.closeQuietly(httpResponse);// daje znac jezeli nie ma responsa
        }
        return null;
    }


    @Override
    public List<ToDo> getToDos() {
        String url = "https://jsonplaceholder.typicode.com/todos/";
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = null;

        try {
            httpResponse = httpClient.execute(httpGet);
            String toDoAsJson = EntityUtils.toString(httpResponse.getEntity());
            List<ToDo> toDos = objectMapper.readValue(
                    toDoAsJson,objectMapper.getTypeFactory().constructCollectionType(List.class,ToDo.class));
            logger.info("Response code: {} for reuest: {}", httpResponse.getStatusLine().getStatusCode(), httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }
        return null;
    }

    @Override
    public void addToDo(ToDo todo) {
        String url = "https://jsonplaceholder.typicode.com/todos/";
        HttpPost httpPost = new HttpPost(url);
        HttpResponse httpResponse = null;
        try {
            String toDoAsJson = objectWriter.writeValueAsString(todo);
            logger.info(toDoAsJson);

            httpPost.setEntity(new StringEntity(toDoAsJson));
            //dodawanie naglowka, header(klucz, wartość)
            httpPost.addHeader("Content-Text","aplication/json");
            httpResponse = httpClient.execute(httpPost);
            logger.info("Response code: {} for reuest: {}", httpResponse.getStatusLine().getStatusCode(), httpPost);
            logger.info(toDoAsJson);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }
    }

    @Override
    public void updateToDo(ToDo todo) {
        String url = "https://jsonplaceholder.typicode.com/todos/"+todo.getId();
        HttpPut httpPut = new HttpPut(url);
        HttpResponse httpResponse = null;

        try {
            String toDoAsJson = objectWriter.writeValueAsString(todo);
            httpPut.setEntity(new StringEntity(toDoAsJson));
            httpPut.addHeader("Content-Text","aplication/json");
            httpResponse = httpClient.execute(httpPut);
            logger.info("Response code: {} for reuest: {}", httpResponse.getStatusLine().getStatusCode(), httpPut);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }
    }
}
