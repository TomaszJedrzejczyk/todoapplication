package pl.sda.example.restclient.http.client.model;

import lombok.Data;

@Data
public class ToDo {
    private long id;
//    @JsonProperty("userId")
    private long userId;
    private String title;
    private boolean completed;

    public ToDo() {
    }

    public ToDo(long userId, String title, boolean completed) {
        this.userId = userId;
        this.title = title;
        this.completed = completed;
    }
}
