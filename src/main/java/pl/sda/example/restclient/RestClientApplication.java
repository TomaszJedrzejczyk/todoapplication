package pl.sda.example.restclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.sda.example.restclient.http.client.model.ToDo;
import pl.sda.example.restclient.service.ToDoService;

import static javafx.scene.input.KeyCode.L;

@SpringBootApplication
public class RestClientApplication implements CommandLineRunner {

    @Autowired
    private ToDoService toDoService;

	public static void main(String[] args) {
		SpringApplication.run(RestClientApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        ToDo toDo = toDoService.getToDo(1L);

        ToDo toDoCreate = new ToDo(1,"title", false);
        toDoService.addToDo(toDoCreate);

        toDoService.getToDos();
    }
}
